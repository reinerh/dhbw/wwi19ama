/** Spielfeldlogik für TicTacToe.
 *
 *  Diese Datei enthält Methoden für den Spielfeld-Datentyp,
 *  die Informationen über den aktuellen Spielzustand liefern.
 */

package main

/// Prüft, ob die Zeile 'row' mit dem Zeichen 'c' gefüllt ist.
func (b * Board) checkRow(row int, c byte) bool {
    for i:=0; i<3; i++ {
        if b[row][i] != c {
            return false
        }
    }
    return true
}

/// Prüft, ob irgendeine Zeile mit dem Zeichen 'c' gefüllt ist.
func (b * Board) checkAllRows(c byte) bool {
    for i:=0; i<3; i++ {
        if b.checkRow(i,c) {
            return true
        }
    }
    return false
}

/// Prüft, ob die Spalte 'col' mit dem Zeichen 'c' gefüllt ist.
func (b * Board) checkColumn(col int, c byte) bool {
    for i:=0; i<3; i++ {
        if b[i][col] != c {
            return false
        }
    }
    return true
}

/// Prüft, ob irgendeine Spalte mit dem Zeichen 'c' gefüllt ist.
func (b * Board) checkAllColumns(c byte) bool {
    for i:=0; i<3; i++ {
        if b.checkColumn(i,c) {
            return true
        }
    }
    return false
}

/// Prüft, ob eine der Diagonalen mit dem Zeichen 'c' gefüllt ist.
func (b * Board) checkDiags(c byte) bool {
    if b[0][0] == c && b[1][1] == c && b[2][2] == c {
        return true
    } 
    if b[0][2] == c && b[1][1] == c && b[2][0] == c {
        return true
    }
    return false
}

/// Prüft, ob der Spieler mit dem Zeichen 'c' das Spiel gewonnen hat.
func (b * Board) checkWinner(c byte) bool {
    if b.checkAllRows(c) || b.checkAllColumns(c) || b.checkDiags(c) {
        return true
    }
    return false
}

/// Liefert die Anzahl der bereits belegten Felder zurück.
func (b * Board) rounds() int {
    result := 0
    for row:=0; row<3; row++ {
        for col:=0; col<3; col++ {
            if b[row][col] != ' ' {
                result++
            }
        }
    }
    return result
}

/// Gibt an, ob das Spiel unentschieden ist.
func (b * Board) draw() bool {
    return b.rounds() == 9
}