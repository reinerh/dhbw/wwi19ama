package main

import "fmt"

func main() {
    run()
}

/// Hauptfunktion für den Spielablauf.
func run() {
    fmt.Println("Nummerierung der Felder:")
    print_board_template()
    fmt.Println()
    
    game := newGame()
    
    for game.isRunning() {
        fmt.Print(game.board)
        game.move()
        game.switchPlayer()
    }

    fmt.Print(game.board)
    game.printResult()
}