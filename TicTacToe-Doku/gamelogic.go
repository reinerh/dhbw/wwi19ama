/** Datentypen und Hilfsfunktionen für das Spiel und den Spieler-Datentyp.
 *
 */

package main

import "fmt"

/// Definition des Spieler-Datentyps
type Player struct {
    name string
    symbol byte
}

/// Definition eines Datentyps, der alle Spiel-Informationen zusammenfasst.
type Game struct {
    p1, p2 Player
    current Player
    board Board
}

/// Konstruktor für ein Spiel: Liefert ein neues Spiel-Objekt mit Spielfeld und Spielern
func newGame() Game {
    var g Game
    g.board.init()
    g.createPlayers('X', 'O')
    g.current = g.p1
    return g
}

/// Erzeugt zwei Spieler mit den angegebenen Zeichen.
/// Fragt dazu die Spieler auf der Konsole nach ihren Namen.
func (g * Game) createPlayers(c1, c2 byte) {
    g.p1.symbol = c1
    fmt.Printf("Spieler %c, bitte geben Sie Ihren Namen ein: ", c1)
    fmt.Scan(&g.p1.name)

    g.p2.symbol = c2
    fmt.Printf("Spieler %c, bitte geben Sie Ihren Namen ein: ", c2)
    fmt.Scan(&g.p2.name)
}

/// Fragt den Spielzug vom aktuellen Spieler ab und führt ihn durch.
func (g * Game) move() {
    var input, row, col int

    // Spielzug abfragen
    fmt.Printf("%v, bitte wählen Sie Ihr Feld: ", g.current.name)
    fmt.Scan(&input)
    input -= 1
    row = input / 3
    col = input % 3
    
    // Falls ungültig, Fehlermeldung und noch einmal fragen.
    if input > 8 || input < 0 || g.board[row][col] != ' ' {
        fmt.Println("Der Zug ist ungültig!")
        g.move()
        return
    }
    
    // Falls gültig, Zug setzen.
    g.board[row][col] = g.current.symbol
}

/// Wechselt den aktuellen Spieler
func (g * Game) switchPlayer() {
    if g.current == g.p1 {
        g.current = g.p2
    } else {
        g.current = g.p1
    }
}

/// Gibt an, ob das Spiel noch läuft.
func (g * Game) isRunning() bool {
    return !g.board.checkWinner('X') && !g.board.checkWinner('O') && !g.board.draw()
}

/// Gibt das Ergebnis des Spiels auf der Konsole aus. 
func (g * Game) printResult() {
    if g.board.checkWinner(g.p1.symbol) {
        fmt.Printf("%v hat gewonnen.\n", g.p1.name)
    } else if g.board.checkWinner(g.p2.symbol) {
        fmt.Printf("%v hat gewonnen.\n", g.p2.name)
    } else if g.board.draw() {
        fmt.Println("Das Spiel endet unentschieden.")
    } else {
        fmt.Println("Das Spiel läuft noch.")
    }
}