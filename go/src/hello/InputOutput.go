package main

import "fmt"

func main() {
    //number()
    //fmt.Println(sum())
    scopes()
}

func number() {
    
    var input int
    fmt.Print("Bitte geben Sie Ihre Lieblingszahl ein: ")
    fmt.Scan(&input)
    fmt.Printf("Sie haben %v eingegeben.\n", input)
}



// Eine Funktion, die den Benutzer so lange nach Zahlen fragt, bis er eine Null eingibt. Anschließend gibt die Funktion die Summe der Zahlen zurück.
func sum() int {
    result := 0
    input := 1
    
    for input != 0 {
        input = read_input()
        result += input
    }
    return result
}

func sum_rek() int {
    input := read_input()
    if input == 0 {
        return 0
    }
    return input + sum_rek()
}





func read_input() int {
    var foo int
    fmt.Printf("Bitte eine Zahl eingeben: ")
    fmt.Scan(&foo)
    return foo
}

func scopes() {
    x := 42
    for i:=0;i<1;i++ {
        x := 38
        
        {
            x:=-42
            fmt.Println(x)
        }
        
        fmt.Println(x)
    }
    fmt.Println(x)
}