package main

import "fmt"

type Board [8][8]byte

func NewBoard() Board {
    var b Board
    
    // Das Feld mit Leerzeichen füllen
    for row:=0; row<len(b); row++ {
        for col:=0; col<len(b[row]); col++ {
            b[row][col] = ' '
        }
    }
    
    return b
}

func (b Board) String() string {
    height := len(b)
    width := len(b[0])
    cell_template:="| %c "
    header_template := "+---"
    
    headerstring := ""
    for col:=0; col < width; col++ {
        headerstring += header_template
    }
    headerstring += "+\n"
    
    result := ""
    for row:=0; row<height; row++ {
        result += headerstring
        for col:=0; col < width; col++ {
            result += fmt.Sprintf(cell_template, b[row][col])
        }
        result += "|\n"
    }
    return result + headerstring
}

func (b Board) allowed(row, col int) bool {
    return b.column_allowed(row, col) && 
           b.diag_left_allowed(row, col) &&
           b.diag_right_allowed(row, col)
}

func (b Board) column_allowed(row, col int ) bool {
    for ; row >= 0; row-- {
        if b[row][col] != ' ' {
            return false
        }
    }
    return true
}

func (b Board) diag_left_allowed(row, col int ) bool {
    for ; row >= 0 && col >= 0; row, col = row-1, col-1 {
        if b[row][col] != ' ' {
            return false
        }
    }
    return true
}

func (b Board) diag_right_allowed(row, col int ) bool {
    for ; row >= 0 && col < len(b[row]); row, col = row-1, col+1 {
        if b[row][col] != ' ' {
            return false
        }
    }
    return true
}


