package main

import "fmt"

func main() {
    b := NewBoard()
    
    b[5][2] = 'D'
    fmt.Print(b)
    
    fmt.Println(b.column_allowed(0,2))
    fmt.Println(b.column_allowed(4,2))
    fmt.Println(b.column_allowed(5,2))
    fmt.Println(b.column_allowed(6,2))
    
    fmt.Println(b.diag_left_allowed(4,1))
    fmt.Println(b.diag_left_allowed(3,0))
    fmt.Println(b.diag_left_allowed(2,5))
    fmt.Println(b.diag_left_allowed(6,3))
    fmt.Println(b.diag_left_allowed(7,4))
    
    fmt.Println(b.diag_right_allowed(6,1))
    fmt.Println(b.diag_right_allowed(7,0))
    
    fmt.Println(b.diag_right_allowed(4,3))
    
    fmt.Println(b.allowed(4,1))
    fmt.Println(b.allowed(4,2))
    fmt.Println(b.allowed(4,3))
    fmt.Println(b.allowed(5,1))
    fmt.Println(b.allowed(5,2))
    fmt.Println(b.allowed(5,3))
    fmt.Println(b.allowed(6,1))
    fmt.Println(b.allowed(6,2))
    fmt.Println(b.allowed(6,3))
    
    
    b2 := NewBoard()
    b2.damen(0)
    fmt.Print(b2)
}

func (b * Board) damen(row int) bool {
    if row >= len(b) {
        return true
    }
    
    for col := 0; col < len(b[row]); col++ {
        if b.allowed(row,col) {
            b[row][col] = 'D'
            if b.damen(row+1) {
                return true
            } else {
                b[row][col] = ' '
            }
        }
    }
    return false
}