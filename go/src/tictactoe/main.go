package main

import "fmt"

type Player struct {
    name string
    symbol byte
}

func main() {
    run()
}

/**
 * Gibt eine Vorlage des Bretts aus, die mit den Zahlen
 * 1 bis 9 gefüllt ist.
 */
func print_template() {
    var b Board
/*    
    var i byte
    for i = '1'; i<='9'; i++ {
        b[(i-49)/3][(i-49)%3] = i
    }
*/
    var i byte = '1'
    for row:=0; row<3; row++ {
        for col:=0; col<3; col++ {
            b[row][col] = i
            i++
        }
    }
    
    fmt.Print(b)
}

func (b Board) set(p Player) Board {
    var input, row, col int
/*
    fmt.Print("Wählen Sie die Zeile: ")
    fmt.Scan(&row)
    fmt.Print("Wählen Sie die Spalte: ")
    fmt.Scan(&col)
*/
    print_template()
    fmt.Print("Bitte wählen Sie das Feld: ")
    fmt.Scan(&input)
    input -= 1
    row = input / 3
    col = input % 3
    
//    if row > 2 || col > 2 || row < 0 || col < 0 || b[row][col] != ' ' {
    if input > 8 || input < 0 || b[row][col] != ' ' {
        fmt.Println("Das war wohl nichts!")
        return b.set(p)
    }
    
    b[row][col] = p.symbol
    return b
}

func run() {    
    // Spielfeld erstellen
    b:= NewBoard()
    fmt.Print(b)
    
    // Spieler erstellen (Name etc.)
    p1 := Player{"Max Mustermann", 'X'}
    p2 := Player{"Martina Musterfrau", 'O'}
    
    player := p1
    rounds := 0
    
    // Solange das Spiel noch läuft
    for !b.checkWinner('X') && !b.checkWinner('O') && rounds < 9 {
        // Eingabe von Spieler abfragen        
        // Spielzug durchführen
        b = b.set(player)
        
        // Board anzeigen
        fmt.Print(b)
        
        // Spieler wechseln
        if player == p1 {
            player = p2
        } else {
            player = p1
        }      
        
        rounds++
    }
    
    // Ausgeben, wer gewonnen hat
    if b.checkWinner('X') {
        fmt.Println("Spieler X gewinnt")
    } else if b.checkWinner('O') {
        fmt.Println("Spieler O gewinnt")
    } else {
        fmt.Println("Unentschieden")
    }
    
    // Evtl. neues Spiel starten
}